<?php
$settings->add(new admin_setting_heading(
            'headerconfig',
            get_string('headerconfig', 'block_gamificationbanner'),
            get_string('descconfig', 'block_gamificationbanner')
        ));

$settings->add(new admin_setting_configcheckbox(
            'gamificationbanner/Allow_Gamification',
            get_string('labelallowhtml', 'block_gamificationbanner'),
            get_string('descallowhtml', 'block_gamificationbanner'),
            '0'
        ));