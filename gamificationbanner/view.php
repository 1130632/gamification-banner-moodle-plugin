<?php
 
require_once('../../config.php');
require_once('gamificationbanner_seemore.php');
require_once("{$CFG->libdir}/formslib.php");
 
global $DB, $OUTPUT, $PAGE;
 
// Check for all required variables.
$courseid = required_param('courseid', PARAM_INT);
$userid = required_param('userid', PARAM_INT);
// Next look for optional variables.
$id = optional_param('id', 0, PARAM_INT);

if (!$course = $DB->get_record('course', array('id' => $courseid))) {
    print_error('invalidcourse', 'block_gamificationbanner', $courseid);
}
require_login($course);

$PAGE->set_url('/blocks/gamificationbanner/view.php', array('id' => $courseid));
$PAGE->set_pagelayout('standard');
$PAGE->set_heading(get_string('seemore', 'block_gamificationbanner'));

// Add navigation breadcrumbs
$settingsnode = $PAGE->settingsnav->add(get_string('settings', 'block_gamificationbanner'));
$editurl = new moodle_url('/blocks/gamificationbanner/view.php', array('id' => $id, 'courseid' => $courseid, 'userid' => $userid));
$editnode = $settingsnode->add(get_string('seemore', 'block_gamificationbanner'), $editurl);
$editnode->make_active();

// seemore form
$seemore = new gamificationbanner_seemore();
// Set hidden form elements
$toform['userid'] = $userid;
$toform['courseid'] = $courseid;
$seemore->set_data($toform);
if ($seemore->is_cancelled()) {
    // Cancelled forms redirect to the course main page.
    $courseurl = new moodle_url('/course/view.php', array('id' => $courseid));
    redirect($courseurl);
} else if ($fromform = $seemore->get_data()) {
    // Code to appropriately act on and store the submitted data
    $courseurl = new moodle_url('/course/view.php', array('id' => $courseid));
    if (!empty($fromform->repbutton)) {
        if (strcmp($fromform->userselect, NO_SELECTION) != 0) {
            $targetuser = $DB->get_record('block_gamificationbanner', array('username' => $fromform->userselect));
            $DB->update_record('block_gamificationbanner', array('id' => $targetuser->id, 'points' => $targetuser->points + REPUTATION_POINTS, 'rep_received' => $targetuser->rep_received + REPUTATION_POINTS));
            $DB->update_record('block_gamificationbanner', array('id' => $fromform->userid, 'rep_given' => 1));
            redirect($courseurl);
        } else {
            $site = get_site();
            echo $OUTPUT->header();
            $seemore->display();
            echo $OUTPUT->footer();
        }
    } else {
        if (empty($fromform->boardselect)) {
            $DB->update_record('block_gamificationbanner', array('id' => $fromform->userid, 'active' => empty($fromform->enablebox) ? 0 : 1));
        } else {
            $DB->update_record('block_gamificationbanner', array('id' => $fromform->userid, 'display_board' => $fromform->boardselect, 'active' => empty($fromform->enablebox) ? 0 : 1));
        }
        redirect($courseurl);
    }
    // Use this to debug
    // print_object($fromform);
} else {
    // form didn't validate or this is the first display
    $site = get_site();
    echo $OUTPUT->header();
    $seemore->display();
    echo $OUTPUT->footer();
}
?>
