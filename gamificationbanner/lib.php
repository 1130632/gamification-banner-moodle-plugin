<?php
const NO_SELECTION = '---';
const REPUTATION_POINTS = 10;
const COMPLETION_POINTS = 10;
const GRADES_MULTIPLIER = 2;

function get_user_points($userdata) {
    global $COURSE, $USER, $DB;
    // Add points for completed activities
    $completioninfo = new completion_info($COURSE);
    $cnt = 0;
    foreach ($completioninfo->get_completions($USER->id, COMPLETION_CRITERIA_TYPE_ACTIVITY) as $completion) {
        if ($completion->is_complete()) {
            $cnt++;
        }
    }
    $points = $cnt * COMPLETION_POINTS;
    // Add points for grades
    $sql = 'SELECT sum(rawgrade) AS grades FROM {grade_grades} WHERE userid = ?';
    $usergrades = $DB->get_record_sql($sql, array($USER->id));
    if (!empty($usergrades)) {
        $points += $usergrades->grades * GRADES_MULTIPLIER;
    }
    // Add reputation points
    if (!empty($userdata)) {
        $points += $userdata->rep_received;
    }
    return round($points);
}

function get_user_class() {
    global $COURSE, $USER;
    $groupid = groups_get_user_groups($COURSE->id, $USER->id)[0][0];
    $class = groups_get_group_name($groupid);
    return $class;
}

function create_table($columnnum, $col1, $col2, $col3, $col4) {
    $table = new html_table();
    $table->width = '70%';
    $table->align = array('left', 'centre');
    switch ($columnnum) {
        case 2:
            $table->size = array($col1, $col2);
            break;
        case 3:
            $table->size = array($col1, $col2, $col3);
            break;
        case 4:
            $table->size = array($col1, $col2, $col3, $col4);
            break;
    }
    return $table;
}
