<?php

namespace block_gamificationbanner\task;

defined('MOODLE_INTERNAL') || die();

/**
 * Task for updating the database of the gamification banner block
 */
class update_db extends \core\task\scheduled_task {

    /**
     * Name for this task.
     *
     * @return string
     */
    public function get_name() {
        return get_string('updatetask', 'block_gamificationbanner');
    }

    /**
     * Update rows from the database table of the gamification banner block
     */
    public function execute() {
        global $DB;
            
        $position = 0;
        $rs = $DB->get_recordset('block_gamificationbanner');
        foreach ($rs as $record) {
            $position++;
            $data = array (
                "id"  => $record->id,
                "last_points" => $record->points,
                "last_position" => $position,
                "rep_given" => 0
            );
            $DB->update_record('block_gamificationbanner', $data);
        }
        $rs->close();
    }
}
