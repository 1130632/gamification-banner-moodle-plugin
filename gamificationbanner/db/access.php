<?php
defined('MOODLE_INTERNAL') || die();

$capabilities = array(
	
    'block/gamificationbanner:myaddinstance' => array(
        'captype' => 'write',
        'contextlevel' => CONTEXT_SYSTEM,
        'archtypes' => array(
            'user' => CAP_ALLOW
        ),
		
        'clonepermissionsfrom' => 'moodle/my:manageblocks'
	
    ),
	
    'block/gamificationbanner:addinstance' => array(
        'riskbitmask' => RISK_SPAM | RISK_XSS,
		
        'captype' => 'write',
        'contextlevel' => CONTEXT_BLOCK,
        'archtypes' => array(
            'editingteacher' => CAP_ALLOW,
            'manager' => CAP_ALLOW
        ),
		
        'clonepermissionsfrom' => 'moodle/site:manageblocks'
    ),
    
    'block/gamificationbanner:viewpages' => array(
 
        'captype' => 'read',
        'contextlevel' => CONTEXT_COURSE,
        'legacy' => array(
            'guest' => CAP_PREVENT,
            'student' => CAP_ALLOW,
            'teacher' => CAP_ALLOW,
            'editingteacher' => CAP_ALLOW,
            'coursecreator' => CAP_ALLOW,
            'manager' => CAP_ALLOW
        )
    ),
 
    'block/gamificationbanner:managepages' => array(
 
        'captype' => 'read',
        'contextlevel' => CONTEXT_COURSE,
        'legacy' => array(
            'guest' => CAP_PREVENT,
            'student' => CAP_PREVENT,
            'teacher' => CAP_PREVENT,
            'editingteacher' => CAP_ALLOW,
            'coursecreator' => CAP_ALLOW,
            'manager' => CAP_ALLOW
        )
    )
);
