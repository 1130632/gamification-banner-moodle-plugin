<?php

/**
 * Task definition for block_gamificationbanner.
 */

defined('MOODLE_INTERNAL') || die();

$tasks = array(
    array(
        'classname' => '\block_gamificationbanner\task\update_db',
        'blocking' => 0,
        'minute' => '0',
        'hour' => '0',
        'day' => '*',
        'month' => '*',
        'dayofweek' => '1',
        'disabled' => 0
    )
);

