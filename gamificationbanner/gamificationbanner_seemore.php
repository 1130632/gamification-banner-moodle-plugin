<?php

require_once("{$CFG->libdir}/formslib.php");
require_once($CFG->dirroot.'/blocks/gamificationbanner/lib.php');
 
class gamificationbanner_seemore extends moodleform {
 
    function definition() {
        global $DB, $USER;
        $userdata = $DB->get_record('block_gamificationbanner', array('username' => $USER->username));
        $mform =& $this->_form;
        
        if ($userdata->active == 1) {
            // add give rep elements
            $mform->addElement('header', 'repheader', get_string('givereputation', 'block_gamificationbanner'));
            $mform->setExpanded('repheader');
            if ($userdata->rep_given == 0) {
                $sql = "SELECT * FROM {block_gamificationbanner} WHERE active = 1 AND username NOT LIKE '".$USER->username."'";
                $searchareas = $DB->get_records_sql($sql);
                $areanames = array();
                $areanames[NO_SELECTION] = NO_SELECTION;
                foreach ($searchareas as $searcharea) {                                                                          
                    $areanames[$searcharea->username] = $searcharea->username;            
                }
                $mform->addElement('autocomplete', 'userselect', get_string('repdesc', 'block_gamificationbanner'), $areanames, array('noselectionstring' => NO_SELECTION));
                $mform->addElement('submit', 'repbutton', get_string('giverep', 'block_gamificationbanner'));
            } else {
                $mform->addElement('static', 'repdesc', get_string('repgivendesc', 'block_gamificationbanner'), '');
            }
            // add leaderboards grouping
            $mform->addElement('header', 'leaderboards', get_string('leaderboards', 'block_gamificationbanner'), null, false);
            $mform->setExpanded('leaderboards');

            $mform->addElement('html', '<h5>'.get_string('generalboard', 'block_gamificationbanner').'</h5>');
            $leaderboard = $this->generate_leaderboard();
            $mform->addElement('html', html_writer::table($leaderboard));
            
            $mform->addElement('html', '<h5>'.get_string('weekboard', 'block_gamificationbanner').'</h5>');
            $wleaderboard = $this->generate_week_leaderboard();
            $mform->addElement('html', html_writer::table($wleaderboard));
            
            $mform->addElement('html', '<h5>'.get_string('classboard', 'block_gamificationbanner').'</h5>');
            $cleaderboard = $this->generate_class_leaderboard();
            $mform->addElement('html', html_writer::table($cleaderboard));
            
            $mform->addElement('select', 'boardselect', get_string('boardselect', 'block_gamificationbanner'), array(get_string('generalboard', 'block_gamificationbanner'), get_string('weekboard', 'block_gamificationbanner'), get_string('classboard', 'block_gamificationbanner')), null);
            $mform->setDefault('boardselect', $userdata->display_board);
        }
        // add disable banner option
        $mform->addElement('header', 'disablebanner', get_string('enabledisablebanner', 'block_gamificationbanner'));
        $mform->setExpanded('disablebanner');
        $mform->addElement('checkbox', 'enablebox', get_string('bannerenabled', 'block_gamificationbanner'));
        $mform->setDefault('enablebox', $userdata->active);

        // hidden elements
        $mform->addElement('hidden', 'userid');
        $mform->addElement('hidden', 'courseid');
        
        $this->add_action_buttons();
    }
    
    // PRIVATE FUNCTIONS
    
    private function generate_leaderboard() {
        global $DB, $USER;
        $sql = 'SELECT * FROM {block_gamificationbanner} WHERE active = 1 ORDER BY points DESC';
        $bannerusers = $DB->get_records_sql($sql);
        $userposition = array_search($USER->username, array_column($bannerusers, 'username')) + 1;
        // Create leaderboard table
        $leaderboard = create_table(4, '15%', '55%', '20%', '10%');
        $leaderboard->head = array(get_string('pos', 'block_gamificationbanner'), get_string('name', 'block_gamificationbanner'), get_string('points', 'block_gamificationbanner'), '');
        // Add items to the leaderboard
        $position = 0;
        foreach ($bannerusers as $banneruser) {
            $position++;
            $icon = '-';
            if ($banneruser->last_position != 0) {
                if ($position < $banneruser->last_position) {
                    $icon = '▲';
                } else if ($position > $banneruser->last_position) {
                    $icon = '▼';
                }
            }
            if ($userposition > 8) {
                if ($position <= 7 || abs($position - $userposition) <= 1) {
                    $leaderboard->data[] = new html_table_row(array($position, $banneruser->username, $banneruser->points, $icon));
                }
                if ($position >= $userposition + 1) {
                    break;
                }
            } else {
                $leaderboard->data[] = new html_table_row(array($position, $banneruser->username, $banneruser->points, $icon));
                if ($position >= 10) {
                    break;
                }
            }
        }
        return $leaderboard;
    }
    
    private function generate_week_leaderboard() {
        global $DB, $USER;
        $sql = 'SELECT * FROM {block_gamificationbanner} WHERE active = 1 ORDER BY points - last_points DESC';
        $bannerusers = $DB->get_records_sql($sql);
        $userposition = array_search($USER->username, array_column($bannerusers, 'username')) + 1;
        // Create leaderboard table
        $leaderboard = create_table(4, '15%', '55%', '20%', '10%');
        $leaderboard->head = array(get_string('pos', 'block_gamificationbanner'), get_string('name', 'block_gamificationbanner'), get_string('weekpoints', 'block_gamificationbanner'), '');
        // Add items to the leaderboard
        $position = 0;
        foreach ($bannerusers as $banneruser) {
            $position++;
            $icon = '-';
            if ($banneruser->last_position != 0) {
                if ($position < $banneruser->last_position) {
                    $icon = '▲';
                } else if ($position > $banneruser->last_position) {
                    $icon = '▼';
                }
            }
            $points = $banneruser->points - $banneruser->last_points;
            if ($userposition > 8) {
                if ($position <= 7 || abs($position - $userposition) <= 1) {
                    $leaderboard->data[] = new html_table_row(array($position, $banneruser->username, $points, $icon));
                }
                if ($position >= $userposition + 1) {
                    break;
                }
            } else {
                $leaderboard->data[] = new html_table_row(array($position, $banneruser->username, $points, $icon));
                if ($position >= 10) {
                    break;
                }
            }
        }
        return $leaderboard;
    }
    
    private function generate_class_leaderboard() {
        global $DB;
        $sql = 'SELECT class, sum(points) AS points FROM {block_gamificationbanner} WHERE active = 1 GROUP BY class ORDER BY points DESC';
        $classes = $DB->get_records_sql($sql);
        // Create leaderboard table
        $leaderboard = create_table(3, '18%', '59%', '23%', null);
        $leaderboard->head = array(get_string('pos', 'block_gamificationbanner'), get_string('class', 'block_gamificationbanner'), get_string('points', 'block_gamificationbanner'));
        // Add items to the leaderboard
        $position = 0;
        foreach ($classes as $class) {
            $position++;
            $leaderboard->data[] = new html_table_row(array($position, $class->class, $class->points));
        }
        return $leaderboard;
    }
}
