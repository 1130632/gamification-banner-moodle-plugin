<?php

class block_gamificationbanner_edit_form extends block_edit_form {
    
    protected function specific_definition($mform) {
        
        // Section header title according to language file.
        $mform->addElement('header', 'config_header', get_string('blocksettings', 'block'));
        
        // A sample string variable with a default value.
        $mform->addElement('text', 'config_title', get_string('blocktitle', 'block_gamificationbanner'));
        $mform->setDefault('config_title', get_string('gamificationbanner', 'block_gamificationbanner'));
        $mform->setType('config_title', PARAM_TEXT);
    }
}