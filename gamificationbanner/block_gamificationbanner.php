<?php

require_once($CFG->dirroot.'/blocks/gamificationbanner/lib.php');

class block_gamificationbanner extends block_list {
    
    private $userdata;
    private $userposition;
    private $bannerusers;
    private $classes;
    
    // First method that is called
    public function init() {
        global $USER, $DB;
        $this->title = get_string('gamificationbanner', 'block_gamificationbanner');

        $this->userdata = $DB->get_record('block_gamificationbanner', array('username' => $USER->username));
        if (empty($this->userdata) || $this->userdata->active != 0) {
            $points = get_user_points($this->userdata);
            if (empty($this->userdata)) {
                $this->userdata = new stdClass();
                $this->userdata->username = $USER->username;
                $this->userdata->class = get_user_class();
                $this->userdata->points = $points;
                $this->userdata->display_board = 0;
                $this->userdata->active = 1;
                $this->userdata->id = $DB->insert_record('block_gamificationbanner', $this->userdata);
            } else {
                if ($this->userdata->points != $points) {
                    $this->userdata->points = $points;
                    $DB->update_record('block_gamificationbanner', $this->userdata);
                }
            }
            $this->get_leaderboard_data();
        }
    }
    
    // Called immediately after init
    public function specialization() {
        if (isset($this->config)) {
            if (empty($this->config->title)) {
                $this->title = get_string('gamificationbanner', 'block_gamificationbanner');
            } else {
                $this->title = $this->config->title;
            }
        }
    }
	
    // Gets the block content to render
    public function get_content() {
        global $COURSE;
        
        if ($this->content !== null) {
            return $this->content;
        }
        $this->content         = new stdClass;
        $this->content->items  = array();
        //$this->content->icons  = array();

        if ($this->userdata->active != 0) {
            $this->generate_leaderboard();
            $this->generate_updates();
            $this->content->items[] = html_writer::empty_tag('br');
            /*
            $this->content->icons[0] = html_writer::empty_tag('img', array('src' => new moodle_url('/pix/i/badge.png'), 'class' => 'icon'));
            $this->content->icons[2] = html_writer::empty_tag('img', array('src' => new moodle_url('/pix/c/event.gif'), 'class' => 'icon'));
            $this->content->icons[5] = html_writer::empty_tag('img', array('src' => new moodle_url('/pix/i/email.gif'), 'class' => 'icon'));
            */
        } else {
            $this->content->items[] = html_writer::tag('h4', get_string('bannerdisabled', 'block_gamificationbanner'));
        }
        $url = new moodle_url('/blocks/gamificationbanner/view.php', array('userid' => $this->userdata->id, 'courseid' => $COURSE->id));
        $this->content->footer = html_writer::link($url, get_string('seemore', 'block_gamificationbanner'));
        
        return $this->content;
    }
    
    // Checks if the block is empty to prevent it from showing
    public function is_empty() {
        $this->get_content();
        return empty($this->content->items);
    }
    
    // Decides if multiple blocks can be added to the page
    public function instance_allow_multiple() {
        return false;
    }
    
    // Decides if the block has global configs
    function has_config() {
        return true;
    }
    
    // Decides if the block header should be hidden
    public function hide_header() {
        return false;
    }
    
    // Decides where the block should appear
    public function applicable_formats() {
        return array(
            'site-index' => false,
            'course-view' => true
        );
    }
    
    // Called when the block is deleted
    public function instance_delete() {
        global $USER, $DB;
        $DB->delete_records('block_gamificationbanner', array('username' => $USER->username));
    }

    // PRIVATE FUNCTIONS
    
    private function generate_leaderboard() {
        // Create title
        $this->content->items[0] = html_writer::tag('h4', get_string('leaderboard', 'block_gamificationbanner'));
        if ($this->userdata->display_board == 2) {
            $leaderboard = $this->generate_classes_leaderboard();
        } else {
            $leaderboard = $this->generate_users_leaderboard();
        }
        // Add leaderbord to the banner
        $this->content->items[1] = html_writer::table($leaderboard);
    }
    
    private function generate_users_leaderboard() {
        // Create leaderboard table
        $leaderboard = create_table(4, '15%', '55%', '20%', '10%');
        if ($this->userdata->display_board == 1) {
            $leaderboard->head = array(get_string('pos', 'block_gamificationbanner'), get_string('name', 'block_gamificationbanner'), get_string('weekpoints', 'block_gamificationbanner'), '');
        } else {
            $leaderboard->head = array(get_string('pos', 'block_gamificationbanner'), get_string('name', 'block_gamificationbanner'), get_string('points', 'block_gamificationbanner'), '');
        }
        // Add items to the leaderboard
        $position = 0;
        foreach ($this->bannerusers as $banneruser) {
            $position++;
            $icon = '-';
            if ($banneruser->last_position != 0) {
                if ($position < $banneruser->last_position) {
                    $icon = '▲';
                } else if ($position > $banneruser->last_position) {
                    $icon = '▼';
                }
            }
            $points = $banneruser->points;
            if ($this->userdata->display_board == 1) {
                $points -= $banneruser->last_points;
            }
            if ($this->userposition > 8) {
                if ($position <= 7 || abs($position - $this->userposition) <= 1) {
                    $leaderboard->data[] = new html_table_row(array($position, $banneruser->username, $points, $icon));
                }
                if ($position >= $this->userposition + 1) {
                    break;
                }
            } else {
                $leaderboard->data[] = new html_table_row(array($position, $banneruser->username, $points, $icon));
                if ($position >= 10) {
                    break;
                }
            }
        }
        return $leaderboard;
    }
    
    private function generate_classes_leaderboard() {
        // Create leaderboard table
        $leaderboard = create_table(3, '18%', '59%', '23%', null);
        $leaderboard->head = array(get_string('pos', 'block_gamificationbanner'), get_string('class', 'block_gamificationbanner'), get_string('points', 'block_gamificationbanner'));
        // Add items to the leaderboard
        $position = 0;
        foreach ($this->classes as $class) {
            $position++;
            $leaderboard->data[] = new html_table_row(array($position, $class->class, $class->points));
        }
        return $leaderboard;
    }
    
    private function generate_updates() {
        global $USER;
        // Create title
        $this->content->items[2] = html_writer::tag('h4', get_string('weeklyupdates', 'block_gamificationbanner'));
        // Create updates table
        $updates = create_table(2, '65%', '35%', null, null);
        // Get updates
        $position = 0;
        $mostimproved; $maximprovement = 0;
        $bestofweek; $maxweekpoints = 0;
        $userlastposition = 0;
        foreach ($this->bannerusers as $banneruser) {
            $position++;
            $positiondif = $banneruser->last_position - $position;
            if ($positiondif >= $maximprovement) {
                $maximprovement = $positiondif;
                $mostimproved = $banneruser;
            }
            $weekpoints = $banneruser->points - $banneruser->last_points;
            if ($weekpoints >= $maxweekpoints && $weekpoints > 0) {
                $maxweekpoints = $weekpoints;
                $bestofweek = $banneruser;
            }
            if (strcmp($banneruser->username, $USER->username) == 0) {
                $userlastposition = $banneruser->last_position;
            }
        }
        if ((empty($mostimproved) || $maximprovement == 0) && !empty($this->bannerusers)) {
            $mostimproved = reset($this->bannerusers);
        }
        // Add updates to the table
        $updates->data[] = new html_table_row(array('<b>'.get_string('bestofweek', 'block_gamificationbanner').'</b>', $bestofweek->username));
        $updates->data[] = new html_table_row(array('<b>'.get_string('mostimproved', 'block_gamificationbanner').'</b>', $mostimproved->username));
        $updates = $this->add_class_updates($updates);
        // Add updates table to the banner
        $this->content->items[3] = html_writer::table($updates);
        $this->generate_user_feedback($userlastposition);
    }
    
    private function add_class_updates($updates) {
        // Get class updates
        $bestofweek; $maxweekpoints = 0;
        foreach ($this->classes as $class) {
            $weekpoints = $class->points - $class->last_points;
            if ($weekpoints >= $maxweekpoints && $weekpoints > 0) {
                $maxweekpoints = $weekpoints;
                $bestofweek = $class;
            }
        }
        $updates->data[] = new html_table_row(array('<b>'.get_string('classofweek', 'block_gamificationbanner').'</b>', $bestofweek->class));
        return $updates;
    }
    
    private function generate_user_feedback($userlastposition) {
        // Give user feedback
        $message = get_string('defaultmessage', 'block_gamificationbanner');
        $userimprovement = $userlastposition - $this->userposition;
        $topquarter = sizeof($this->bannerusers) / 4;
        if ($this->userposition <= $topquarter) {
            $message = get_string('youaretopquarter', 'block_gamificationbanner');
        } else if ($userimprovement > 0) {
            $message = get_string('youimproved', 'block_gamificationbanner');
        } else if ($userimprovement == 0) {
            $message = get_string('youstagnated', 'block_gamificationbanner');
        }
        // Add feedback to the banner
        $this->content->items[4] = html_writer::tag('center', '<b>'.$message.'</b>');
    }
    
    private function get_leaderboard_data() {
        global $USER, $DB;
        if ($this->userdata->display_board == 1) {
            $sql = 'SELECT * FROM {block_gamificationbanner} WHERE active = 1 ORDER BY points - last_points DESC';
        } else {
            $sql = 'SELECT * FROM {block_gamificationbanner} WHERE active = 1 ORDER BY points DESC';
        }
        $this->bannerusers = $DB->get_records_sql($sql);
        $this->userposition = array_search($USER->username, array_column($this->bannerusers, 'username')) + 1;
        
        $classsql = 'SELECT class, sum(points) AS points, sum(last_points) AS last_points FROM {block_gamificationbanner} WHERE active = 1 GROUP BY class ORDER BY points DESC';
        $this->classes = $DB->get_records_sql($classsql);
    }

}